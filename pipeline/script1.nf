/*
 * define the different parameters 
 */
params.reads="$projectDir/reads/CRR194192.fq" 
params.genome="$projectDir/NCBIM37.genome.fa" 
params.annotation_gene="$projectDir/gencode.vM1.annotation.gtf" 
params.annotation_circARN="$projectDir/mmu_mm9_circRNA.bed" 
params.outdir="results"

log.info """\
      NF-PIPELINE
    ===================================
    annotation_gene: ${params.annotation_gene}
    reads        : ${params.reads}
    genome       : ${params.genome}
    outdir       : ${params.outdir}
    annotation_circARN : ${params.annotation_circARN}
    """
    .stripIndent(true)

/*
 * Define the isoCirc process
 */
process ISOCIRC {
    publishDir params.outdir,mode:'copy'
    input:
    path reads from params.reads
    path genome from params.genome
    path annotation_gene from params.annotation_gene
    path annotation_circARN from params.annotation_circARN

    output:
    path 'output_CRR194192'
 
    script:
    """
    isocirc -t 10 ${reads} ${genome} ${annotation_gene} ${annotation_circARN} output_CRR194192
    """
}

